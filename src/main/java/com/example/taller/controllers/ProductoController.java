package com.example.taller.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.taller.entities.Producto;
import com.example.taller.models.AjaxResponseProductos;
import com.example.taller.services.ProductoService;

@Controller
@RequestMapping("/producto")
public class ProductoController {

	@Autowired
	ProductoService productoService;

	@GetMapping("/")
	public String index() {
		return "index";
	}

	@PostMapping("/crear")
	@ResponseBody
	public ResponseEntity<AjaxResponseProductos> crear(@RequestBody Producto producto) {

		AjaxResponseProductos resultado = new AjaxResponseProductos();

		try {

			Producto prod = productoService.crear(producto);

			resultado.setProducto(prod);

		} catch (Exception e) {
			resultado.setMensaje(e.getMessage());

			return ResponseEntity.badRequest().body(resultado);
		}

		return ResponseEntity.ok(resultado);

	}

	@GetMapping("/listar")
	@ResponseBody
	public ResponseEntity<AjaxResponseProductos> listar() {

		AjaxResponseProductos resultado = new AjaxResponseProductos();

		try {

			List<Producto> productos = productoService.listar();

			resultado.setProductos(productos);

		} catch (Exception e) {
			resultado.setMensaje(e.getMessage());

			return ResponseEntity.badRequest().body(resultado);
		}

		return ResponseEntity.ok(resultado);

	}
	
	@GetMapping("/buscar/{nombre}")
	@ResponseBody
	public ResponseEntity<AjaxResponseProductos> listarPorNombre(@PathVariable String nombre) {

		AjaxResponseProductos resultado = new AjaxResponseProductos();

		try {

			List<Producto> productos = productoService.listarPorNombre(nombre);

			resultado.setProductos(productos);

		} catch (Exception e) {
			resultado.setMensaje(e.getMessage());

			return ResponseEntity.badRequest().body(resultado);
		}

		return ResponseEntity.ok(resultado);

	}
}
