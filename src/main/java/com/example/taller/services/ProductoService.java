package com.example.taller.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.taller.entities.Producto;
import com.example.taller.repositories.ProductoRepository;

@Service
public class ProductoService {
	
	@Autowired
	ProductoRepository productoRepository;

	
	public Producto crear(Producto producto) throws Exception {
		
		if(producto.getNombre().isEmpty() || producto.getNombre() == null) {
			throw new Exception("el nombre del producto no puede estar vacío");
		}
		
		return productoRepository.save(producto);
	}


	public List<Producto> listar() {
		
		return productoRepository.findAll();
	}


	public List<Producto> listarPorNombre(String nombre) {
		
		return productoRepository.listarPorNombre("%" + nombre + "%");
	}
	
	

}
