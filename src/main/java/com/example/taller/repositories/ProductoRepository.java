package com.example.taller.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.taller.entities.Producto;

@Repository
public interface ProductoRepository extends JpaRepository<Producto, String>, PagingAndSortingRepository<Producto, String>{

	@Query("SELECT p from Producto p WHERE p.nombre LIKE :q ORDER BY p.nombre ASC")
	List<Producto> listarPorNombre(@Param("q") String nombre);
	
}
