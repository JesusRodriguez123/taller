package com.example.taller.models;

import java.util.List;

import com.example.taller.entities.Producto;

import lombok.Data;

@Data
public class AjaxResponseProductos {

    String mensaje;
    List<Producto> productos;
    Producto producto;
}
