package com.example.taller.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
public class Producto {
	
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	
	private String nombre;
	
	private String precio;
	
	public Producto() {
		
	}
	
    public Producto(String nombre, String precio) {
        this.nombre = nombre;
        this.precio = precio;
    }
	

}
